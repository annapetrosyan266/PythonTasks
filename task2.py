import string

def foo(str:string) :
 vowel = set("AEIUOaeiuo")
 tmp = 0 
 for x in str:
  if x in vowel:
   tmp += 1     
 return tmp

str = "manyVowels"
  
print("Number of Vowels:",foo(str)) 