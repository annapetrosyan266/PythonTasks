from collections import Counter

x = 0
arr = []
while True:
    if x == -1:
        break
    x = int(input())
    arr.append(x)
    
c = Counter(arr);
result = c.most_common(1)
print(f"Most common element is {result[0][0]}")
print(f"It repeats {result[0][1]} times")

    